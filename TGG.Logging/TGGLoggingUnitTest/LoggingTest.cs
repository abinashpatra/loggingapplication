﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TGG.Logging;
using System.Collections.Generic;
using System.Transactions;

namespace TGGLoggingUnitTest
{
    [TestClass]
    public class LoggingTest
    {
        [TestMethod]
        public void LogWithoutLog4Net()
        {
            for (int i = 0; i < 1000; i++)
            {
                // TGGSimpleLogger.Initialize();
                TGGSimpleLogger.Log(TGGLoggingLevel.Error, DateTime.UtcNow, "TEST LOGGING CLASS 1", "EXCEPTION MESSAGE", null, "TEST APP NAME", null);
            }
        }

         [TestMethod]
        public void LogWithDictionaryInput()
        {
            for (int i = 0; i < 1000; i++)
            {
                // TGGSimpleLogger.Initialize();
                Dictionary<string, string> log = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
                log.Add("Level", TGGLoggingLevel.Error.ToString());
                log.Add("LogDate", DateTime.UtcNow.ToString());
                log.Add("Logger", "TEST LOGGING CLASS  - Main");
                log.Add("Message", "Exception Message \\t  \\n Logging Message");
                log.Add("appName", "Test");
                TGGSimpleLogger.Log(log);
                //TGGSimpleLogger.LogWithoutLog4Net(TGGLoggingLevel.Error, DateTime.UtcNow, "TEST LOGGING CLASS 1", "EXCEPTION MESSAGE", null, "TEST APP NAME", null);
            }
        }


         [TestMethod]
         public void LogWithLog4Net()
         {
             using (TransactionScope tran = new TransactionScope())
             {
                 TGGLogger.Initialize(AppDomain.CurrentDomain.BaseDirectory + "\\log4Net.config");
                 try
                 {
                     //tran.GetType().GUID
                     //GLogger.Log(LoggingLevel.Info, "Calling method Divide by zero. start", new { User = "Abinash", TransactionId = tran.GetType().GUID });
                     TGGLogger.Log("METHODNAME - LogTest.Main", TGGLoggingLevel.Info, "Calling method Divide by zero. start", new { User = "Abinash", TransactionId = tran.GetType().GUID.ToString() });
                     int i = 25;
                     int j = 0;
                     int foo = i / j;
                     //TGGLogger.Log(LoggingLevel.Info, "Calling method FooDivide by zero. end", new { User = "Abinash", TransactionId = tran.GetType().GUID });
                     TGGLogger.Log("METHODNAME - LogTest.Main", TGGLoggingLevel.Info, "Calling method Divide by zero. end", new { User = "Abinash", TransactionId = tran.GetType().GUID.ToString() });
                     tran.Complete();
                 }
                 catch (DivideByZeroException ex)
                 {
                     TGGLogger.Log("METHODNAME - LogTest.Main", TGGLoggingLevel.Error, ex.Message,
                     new { User = "Abinash", TransactionId = tran.GetType().GUID }, ex);

                 }
                 finally
                 {
                     tran.Dispose();
                 }
             }
         }

    }
}

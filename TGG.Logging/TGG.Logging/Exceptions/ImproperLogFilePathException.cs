﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGG.Logging.Exceptions
{
    /// <summary>
    /// Thrown when a user attempts to write log to in an invalid file path
    /// </summary>
    public class LogFilePathNotMentioned : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public LogFilePathNotMentioned(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">The error message.</param>
        public LogFilePathNotMentioned(string message)
            : this(message, null)
        {
        }
    }
}

﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TGG.Logging
{
    public class TGGLogger
    {
        private static bool isInitialized;

        public static void Initialize()
        {
            Initialize(null);
        }

        public static void Initialize(string configFile)
        {
            if (!isInitialized)
            {
                if (!String.IsNullOrEmpty(configFile))
                    XmlConfigurator.ConfigureAndWatch(new FileInfo(configFile));

                    //XmlConfigurator.Configure(ConfigurationManager.GetSection("applicationSettings/log4net") as XmlElement);
                else
                    XmlConfigurator.Configure();
                isInitialized = true;
            }
            else
                throw new LoggingInitializationException(
                      "Logging has already been initialized.");
        }

        private static void LogBase(ILog log, TGGLoggingLevel loggingLevel,
        string message, object loggingProperties, Exception exception)
        {
            if (ShouldLog(log, loggingLevel))
            {
                PushLoggingProperties(loggingProperties);
                switch (loggingLevel)
                {
                    //case LoggingLevel.Debug: log.Debug(message, exception); break;
                    //case LoggingLevel.Info: log.Info(message, exception); break;
                    //case LoggingLevel.Warning: log.Warn(message, exception); break;
                    //case LoggingLevel.Error: log.Error(message, exception); break;
                    //case LoggingLevel.Fatal: log.Fatal(message, exception); break;

                    case TGGLoggingLevel.Debug: Task.Run(() => log.Debug(message, exception)); break;
                    case TGGLoggingLevel.Info: Task.Run(() => log.Info(message, exception)); break;
                    case TGGLoggingLevel.Warning: Task.Run(() => log.Warn(message, exception)); break;
                    case TGGLoggingLevel.Error: Task.Run(() => log.Error(message, exception)); break;
                    case TGGLoggingLevel.Fatal: Task.Run(() => log.Fatal(message, exception)); break;

                       
                }
                PopLoggingProperties(loggingProperties);
            }
        }

        private static bool ShouldLog(ILog log, TGGLoggingLevel loggingLevel)
        {
            switch (loggingLevel)
            {
                case TGGLoggingLevel.Debug: return log.IsDebugEnabled;
                case TGGLoggingLevel.Info: return log.IsInfoEnabled;
                case TGGLoggingLevel.Warning: return log.IsWarnEnabled;
                case TGGLoggingLevel.Error: return log.IsErrorEnabled;
                case TGGLoggingLevel.Fatal: return log.IsFatalEnabled;
                default: return false;
            }
        }

        // C#
        private static void PushLoggingProperties(object loggingProperties)
        {
            if (loggingProperties != null)
            {
                Type attrType = loggingProperties.GetType();
                PropertyInfo[] properties = attrType.GetProperties(
                               BindingFlags.Public | BindingFlags.Instance);
                for (int i = 0; i < properties.Length; i++)
                {
                    object value = properties[i].GetValue(loggingProperties, null);
                    if (value != null)
                        ThreadContext.Stacks[properties[i].Name].Push(value.ToString());
                }
            }
        }

        private static void PopLoggingProperties(object loggingProperties)
        {
            if (loggingProperties != null)
            {
                Type attrType = loggingProperties.GetType();
                PropertyInfo[] properties = attrType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                for (int i = properties.Length - 1; i >= 0; i--)
                {
                    object value = properties[i].GetValue(loggingProperties, null);
                    if (value != null)
                        ThreadContext.Stacks[properties[i].Name].Pop();
                }
            }
        }

        /// <summary>
        /// Logs an entry to all logs.
        /// </summary>
        /// <param name="loggingLevel">The logging level.</param>
        /// <param name="message">The message.</param>
        /// <exception cref="LoggingInitializationException">Thrown if logger has not been initialized.</exception>
        public static void Log(TGGLoggingLevel loggingLevel, string message)
        {
            Log(loggingLevel, message, null, null);
        }

        /// <summary>
        /// Logs an entry to all logs.
        /// </summary>
        /// <param name="loggingLevel">The logging level.</param>
        /// <param name="message">The message.</param>
        /// <param name="loggingProperties">Any additional properties for the log as defined in the logging configuration.</param>
        /// <exception cref="LoggingInitializationException">Thrown if logger has not been initialized.</exception>
        public static void Log(TGGLoggingLevel loggingLevel, string message, object loggingProperties)
        {
            
            Log(loggingLevel, message, loggingProperties, null);
        }

        /// <summary>
        /// Logs an entry to all logs.
        /// </summary>
        /// <param name="loggingLevel">The logging level.</param>
        /// <param name="message">The message.</param>
        /// <param name="loggingProperties">Any additional properties for the log as defined in the logging configuration.</param>
        /// <param name="exception">Any exception to be logged.</param>
        public static void Log(TGGLoggingLevel loggingLevel, string message, object loggingProperties, Exception exception)
        {
            
            foreach (ILog log in GetLeafLoggers())
                LogBase(log, loggingLevel, message, loggingProperties, exception);
        }

        /// <summary>
        /// Logs an entry to the specified log.
        /// </summary>
        /// <param name="logName">The name of the log.</param>
        /// <param name="loggingLevel">The logging level.</param>
        /// <param name="message">The message.</param>
        /// <exception cref="InvalidLogException">Thrown if <paramref name="logName"/> does not exist.</exception>
        public static void Log(string logName, TGGLoggingLevel loggingLevel, string message)
        {
            //System.Threading.Thread.Sleep(4000);
            Log(logName, loggingLevel, message, null, null);
        }

        /// <summary>
        /// Logs an entry to the specified log.
        /// </summary>
        /// <param name="logName">The name of the log.</param>
        /// <param name="loggingLevel">The logging level.</param>
        /// <param name="message">The message.</param>
        /// <param name="loggingProperties">Any additional properties for the log as defined in the logging configuration.</param>
        /// <exception cref="InvalidLogException">Thrown if <paramref name="logName"/> does not exist.</exception>
        public static void Log(string logName, TGGLoggingLevel loggingLevel, string message, object loggingProperties)
        {
            
            //Log(logName, loggingLevel, message, loggingProperties, null);
            Task.Run(() => Log(logName, loggingLevel, message, loggingProperties, null));
        }

        /// <summary>
        /// Logs an entry to the specified log.
        /// </summary>
        /// <param name="logName">The name of the log.</param>
        /// <param name="loggingLevel">The logging level.</param>
        /// <param name="message">The message.</param>
        /// <param name="loggingProperties">Any additional properties for the log as defined in the logging configuration.</param>
        /// <param name="exception">Any exception to be logged.</param>
        /// <exception cref="InvalidLogException">Thrown if <paramref name="logName"/> does not exist.</exception>
        public static void Log(string logName, TGGLoggingLevel loggingLevel, string message, object loggingProperties, Exception exception)
        {
           // System.Threading.Thread.Sleep(4000);
            ILog log = LogManager.GetLogger(logName);
            if (log != null)
                LogBase(log, loggingLevel, message, loggingProperties, exception);
            //else
            //    throw new InvalidLogException("The log \"" + logName + "\" does not exist or is invalid.", logName);
        }


        private static IEnumerable<ILog> GetLeafLoggers()
        {
            ILog[] allLogs = LogManager.GetCurrentLoggers();
            IList<ILog> leafLogs = new List<ILog>();
            for (int i = 0; i < allLogs.Length; i++)
            {
                bool isParent = false;
                for (int j = 0; j < allLogs.Length; j++)
                {
                    if (i != j && allLogs[j].Logger.Name.StartsWith(string.Format("{0}.", allLogs[i].Logger.Name)))
                    {
                        isParent = true;
                        break;
                    }
                }
                if (!isParent)
                    leafLogs.Add(allLogs[i]);
            }
            return leafLogs;
        }
    }
}

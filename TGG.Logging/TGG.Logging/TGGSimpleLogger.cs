﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;
using TGG.Logging.Exceptions;
using System.Threading;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Reflection;

namespace TGG.Logging
{
    public class TGGSimpleLogger
    {
        //private static ReaderWriterLockSlim lockFile = new ReaderWriterLockSlim();
        //private static bool isInitialized;
        //public static void Initialize()
        //{
        //    Initialize(null);
        //}

        //public static void Initialize(string configFile)
        //{

        //    //if (!isInitialized)
        //    //{
        //    //    isInitialized = true;
        //    //    lockFile = new ReaderWriterLockSlim();
        //    //}
        //}

        public static void Log(TGGLoggingLevel level,
                                              DateTime logDate,
                                              string logger,
                                              string message,
                                              string exception,
                                              string appName,
                                              string transactionId)
        {

            TGGMessage logMessage = new TGGMessage()
            {
                Level = level,
                Logger = logger,
                Message = message,
                Exception = exception,
                AppName = appName,
                TransactionId = transactionId
            };
            string jsonMessage = JsonConvert.SerializeObject(logMessage);
            WriteLogFile(jsonMessage);
            //Task.Run(() => WriteLogFile(logMessage));




        }

        public static void Log(Dictionary<string, string> logInfo)
        {
            //Incase disctionary does not contain appname, add it here
            string appname = ConfigurationManager.AppSettings["AppName"] ?? string.Empty;
            if (!logInfo.ContainsKey("AppName"))
            {
                logInfo.Add("AppName", appname);
            }
            //Incase disctionary does not contain logdate, add it here
            if (!logInfo.ContainsKey("LogDate"))
            {
                logInfo.Add("LogDate", DateTime.UtcNow.ToString());
            }
            WriteLogFile(DictionaryToJson(logInfo));

        }

        private static void WriteLogFile(string jsonMessage)
        {

            //Read the path from the config file
            string pathName = ConfigurationManager.AppSettings["LogFilePath"];
            string appname = ConfigurationManager.AppSettings["AppName"];
            string extensionName = ConfigurationManager.AppSettings["ExtensionName"];


            //null check
            if (String.IsNullOrEmpty(pathName))
            {
                throw new LogFilePathNotMentioned("Looks like the log file path is not mentioned in the config file. Make sure the config file has a valid entry for  ''LogFilePath'' ");
            }
            //Permission check
            else if (CheckPermission(Path.GetDirectoryName(pathName)))
            {
                StringBuilder finalFileName = new StringBuilder();
                finalFileName.Append(Path.GetDirectoryName(pathName));
                finalFileName.Append("\\");
                finalFileName.Append(appname);
                finalFileName.Append(DateTime.Now.ToString("MMddyyyy"));
                finalFileName.Append(extensionName);
                //Write the message and close the file
                //using (System.IO.StreamWriter file = new System.IO.StreamWriter((finalFileName.ToString(), true))
                //{
                //    file.WriteLine(jsonMessage);
                //}



                //lockFile.EnterWriteLock();
                //try
                //{
                //    using (StreamWriter sw = File.AppendText(finalFileName.ToString()))
                //    {
                //        sw.WriteLine(jsonMessage);
                //    }
                //}
                //finally
                //{
                //    lockFile.ExitWriteLock();
                //}







                // get application GUID as defined in AssemblyInfo.cs
                string appGuid = ((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), false).GetValue(0)).Value.ToString();

                // unique id for global mutex - Global prefix means it is global to the machine
                string mutexId = string.Format("Global\\{{{0}}}", appGuid);

                // Need a place to store a return value in Mutex() constructor call
                bool createdNew;

                var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
                var securitySettings = new MutexSecurity();
                securitySettings.AddAccessRule(allowEveryoneRule);


                using (var mutex = new Mutex(false, mutexId, out createdNew, securitySettings))
                {

                    var hasHandle = false;
                    try
                    {
                        try
                        {
                            // note, you may want to time out here instead of waiting forever
                            // mutex.WaitOne(Timeout.Infinite, false);
                            hasHandle = mutex.WaitOne(5000, false);
                            if (hasHandle == false)
                                throw new TimeoutException("Timeout waiting for exclusive access");
                        }
                        catch (AbandonedMutexException)
                        {
                            // Log the fact the mutex was abandoned in another process, it will still get aquired
                            hasHandle = true;
                        }

                        using (StreamWriter sw = File.AppendText(finalFileName.ToString()))
                        {
                            sw.WriteLine(jsonMessage);
                        }
                    }
                    finally
                    {
                        if (hasHandle)
                            mutex.ReleaseMutex();
                    }
                }






                //Object lockObj = new Object();
                //if (Monitor.TryEnter(lockObj))
                //{
                //    try
                //    {
                //        using (StreamWriter sw = File.AppendText(finalFileName.ToString()))
                //        {
                //            sw.WriteLine(jsonMessage);
                //        }
                //    }
                //    finally
                //    {
                //        Monitor.Exit(lockObj);
                //    }
                //}

            }
            else
            {
                throw new UnauthorizedAccessException("The credentials used to access the log file are improper");
            }
        }

        private static bool CheckPermission(string directoryName)
        {
            bool hasPermission = false;
            try
            {

                using (new TGG.Logging.Impersonator.Impersonator(ConfigurationManager.AppSettings["UserId"], ConfigurationManager.AppSettings["Domain"], ConfigurationManager.AppSettings["Password"]))
                {
                    if (Directory.Exists(directoryName))
                    {
                        hasPermission = true;
                    }
                }
            }
            catch (Exception)
            {

                hasPermission = false;
            }

            return hasPermission;
        }

        private static string DictionaryToJson(Dictionary<string, string> logInfo)
        {
            
            var entries = logInfo.Select(d =>
                string.Format("\"{0}\": [{1}]", d.Key, d.Value));
            return "{" + string.Join(",", entries) + "}";
        }

    }
}

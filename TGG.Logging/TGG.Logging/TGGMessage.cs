﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGG.Logging
{
    public class TGGMessage
    {
        public TGGLoggingLevel Level { get; set; }
        public DateTime LogDate { get; set; }
        public string Logger { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public string AppName { get; set; }
        public string TransactionId { get; set; }
    }

}